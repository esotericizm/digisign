var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Document = require('./models/documents');
var routes = require('./routes/index');

var dbString = 'mongodb://DigiSign:DigiSign2015!@ds059654.mongolab.com:59654/heroku_0qltw110';
mongoose.connect(dbString, function() {
  console.log('connected');
});


var routes = require('./routes/index');

var app = express();


var app = express();
var server = require('http').createServer(app);
var socketio = require('socket.io')(server, {
  path: '/socket.io'
});

socketio.set('origins', '*:*');

app.use(function (req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

require('./express')(socketio, app);
require('./socketio')(socketio, app);
require('./routes')(app);

  //app.get('*', function(req, res) {
  //  res.render('404')
  //});


  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handlers

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });

server.listen(process.env.PORT || 3000, '0.0.0.0', function () {
  console.log('Express server listening on %d, in %s mode', 3000, app.get('env'));
});

module.exports = app;
