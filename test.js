var Peer = require('bitcore-p2p').Peer;
var Messages = require('bitcore-p2p').Messages

var peer = new Peer({host: '127.0.0.1'});

peer.on('ready', function() {
  // peer info
  console.log(peer)
  console.log(peer.version, peer.subversion, peer.bestHeight);
  var messages = new Messages();
  message = messages.Transaction('010000000157c83bd0f85766badb678aa0e19ce5094cf9609441c09d59b7dabee193f760d80000000000ffffffff0200e1f505000000001976a914654aec23d5936f1b4bac34466312464e6186a38c88ac0000000000000000426a406461306262633239323064653864303335613038376131303735356466306231386639333132386638396234393431323361663162336364336362626137323000000000');
  peer.sendMessage(message)
  console.log(message);
});

peer.on('inv', function(message) {
   console.log(message);
});

peer.on('tx', function(message) {
    console.log(message);
});

peer.on('addr', function(message) {
    console.log(message);
});

peer.on('disconnect', function() {
  console.log('connection closed');
});

peer.connect();
