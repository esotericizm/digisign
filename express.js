
'use strict';

var express = require('express');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var path = require('path');


module.exports = function(socketio, app) {
  app.set('views', path.resolve(__dirname, './views'));
  app.set('view engine', 'ejs');
// Enable Access control headers
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());    
  app.use(express.static(path.resolve(__dirname, './public')));
};
