$(document).ready(function() {
	var socket = io.connect('/');
	socket.emit('connected');

	$('#Submit').click(function(){
		$('#alert').addClass('hidden');
		$('#loadingSpinner').removeClass('hidden');
		$('#searchResults').addClass('hidden');
		$('#convertedTxResults').addClass('hidden');
		var searchText = $('#searchText').val();
		socket.emit('search', searchText);
	});

	socket.on('onSearch', function(doc){
		$('#loadingSpinner').addClass('hidden');
		if(!doc.found){
			$('#alert').html('Could not find any matches');
			$('#alert').removeClass('hidden')
			$('#alert').addClass('alert alert-error')
		} else if (doc.txConversion){
			$('#convertedTxResults').removeClass('hidden');
			$('#convertedTx').html(doc.data);
		} else {
			$('#searchResults').removeClass('hidden');
			for (var i = 0; i < doc.data.length; i++){
				$('#docFound').append('<tr><td><a href="/document/' + doc.data[i].documentMessage + '">' + doc.data[i].documentMessage + '</a></td><td>' + doc.data[i].documentType + '</td><td>' + doc.data[i].txSent  + '</td><td>' + new Date(doc.data[i].timeAdded * 1000) + '</td><td>' + new Date(doc.data[i].timeBroadcasted * 1000) + '</td></tr>');
			}
		}
	})
});