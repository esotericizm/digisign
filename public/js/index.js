$(document).ready(function() {
	var prevArr = [];
	var socket = io.connect('/');
	socket.emit('connected');

	socket.on('docFound', function(json){
		console.log(json)
		window.location.replace("/document/"+json.docID);
	});

	socket.on('foundPrevious', function(data){
		if(data === "Document Not Found"){
			$('#alert').html(data);
			$('#alert').addClass('alert alert-error');
			setTimeout(function() {
				$('#alert').html('');
				$('#alert').removeClass('alert alert-error');		
			}, 5000);
		} else {
		prevArr.push(data);
		$('#addContent div:last').before('<div id="uploadDivNew" class="col-sm-12 text-left" style="margin-left:55px; margin-top:15px"><label class="text-left" for="PreviousDoc" >Previous Document</label><pre id="PreviousDoc" style="min-height:40px; margin-right:100px; max-width:60%; float:right">' + data + '</pre></div>');			
		}
	});
	/*$('#docReference').click(function(e){
		e.preventDefault();
		$('#addContent div:last').before('<div id="uploadDivNew" class="col-sm-12" style="margin-top:15px"><input style="float:right; margin-right:100px" type="file" id="Previous Document" ></div>');
	});*/


	// uncomment this to try non-HTML support:
	//window.File = window.FileReader = window.FileList = window.Blob = null;
	
	var html5 = window.File && window.FileReader && window.FileList && window.Blob;

	// 11 Digit unique ID to store the sha hash/message in mongo.
	function makeid()
	{
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for( var i=0; i < 20; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	}

	$('#txBtn').click(function(e){
		var txid = $('#checkTX').val();
		socket.emit('checkTx', txid);
	});

	$('#docSubmit').click(function(e){
		var id = makeid();
		var docType = $('#docType').find(":selected").val();
		var hash = $('#dochash').html();
		var docName = null;
		if($('#docName').val().length > 0){
			docName = $('#docName').val();
		}
		var isPrivate = $('#private').is(':checked');
		var json = { "id": id, "docHash": hash, name: docName, "docType": docType, "previousDocs": prevArr, "isPrivate": isPrivate };
		socket.emit('newDoc', json);
	});

	$('#messageSubmit').click(function(){
		var message = $("#docMessage").val();
		if (message.length > 80) {
			$('#charAlert').removeClass('hidden');
			$('#charAlert').addClass('alert alert-error');
			$('#charAlert').html('Message has a maximum length of 80 bytes');
		} else {
			$('#charAlert').addClass('hidden');
			var hash = CryptoJS.SHA256(message, crypto_callback, message_finish);
		}
	})


	var crypto_callback = function(p) {
		var w = ((p*100).toFixed(0));
	}
	
	var crypto_finish = function(hash) {
		$('#dochash').html(hash.toString());
	}

	var message_finish = function(hash) {
		var id = makeid();
		var isPrivate = $('#privateMsg').is(':checked');
		var message = $("#docMessage").val();
		socket.emit('newMsg', {id: id, hash: hash.toString(), message: message, isPrivate: isPrivate });
	}

	var previous_finish = function(hash) {
		socket.emit('addPrevious', hash.toString())
	}
	
	function handleFileSelect(f) {
		if (!html5) {
			return;
		}
	    var output = "";
	    output = 'Preparing to hash ' + escape(f.name)
			+ ' (' + (f.type || 'n/a') + ') - '
			+ f.size + ' bytes, last modified: '
			+ (f.lastModifiedDate ? f.lastModifiedDate
					.toLocaleDateString() : 'n/a' )+ '';
	    
	    var reader = new FileReader();
		reader.onload = function(e) {
			var data = e.target.result;
			console.log('lol')
			setTimeout(function() {
				CryptoJS.SHA256(data,crypto_callback,crypto_finish);
			}, 200);
			
		};
		reader.onprogress = function(evt) {
		    if (evt.lengthComputable) {
		    	var w = (((evt.loaded / evt.total)*100).toFixed(2));
		    }
		}
		reader.readAsBinaryString(f);
	}
	
	function handlePreviousFile(f) {
		if (!html5) {
			return;
		}
	    var output = "";
	    output = 'Preparing to hash ' + escape(f.name)
			+ ' (' + (f.type || 'n/a') + ') - '
			+ f.size + ' bytes, last modified: '
			+ (f.lastModifiedDate ? f.lastModifiedDate
					.toLocaleDateString() : 'n/a' )+ '';
	    
	    var reader = new FileReader();
		reader.onload = function(e) {
			var data = e.target.result;
			console.log('lol')
			setTimeout(function() {
				CryptoJS.SHA256(data,crypto_callback,previous_finish);
			}, 200);
			
		};
		reader.onprogress = function(evt) {
		    if (evt.lengthComputable) {
		    	var w = (((evt.loaded / evt.total)*100).toFixed(2));
		    }
		}
		reader.readAsBinaryString(f);
	}

	document.getElementById('file').addEventListener('change', function(evt) {
		var f = evt.target.files[0];
		handleFileSelect(f);
	}, false);

	document.getElementById('file1').addEventListener('change', function(evt) {
		$('#alert').html('');
		$('#alert').removeClass('alert alert-error');
		var f = evt.target.files[0];
		handlePreviousFile(f);
	}, false);


});