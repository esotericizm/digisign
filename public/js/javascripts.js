/*
 * Main Controller file for invoking
 * other jQuery Plugin.
 * 
 * This file will be run upon document ready state
 * and some of the function will only be invoked
 * during windows element fully loaded state.
 * 
 * @author jason.xie@victheme.com
 */
jQuery(document).ready(function($) {

    "use strict";

    /**
     * Global Variable
     */
    var Window = $(window),
        Document = $(this),
        Html = $('html'),
        Body = $('body'),
        MainContent = $('#maincontent'),
        Isotope = $('.isotope-sources'),
        Galleries = $('.galleries'),
        Maps = $('#maps-element'),
        EqualRow = $('.equalheightRow'),
        Liquid = $('.liquid'),
        DotLine = $('.dotline'),
        RoundThumbs = $('.post-round-thumbnail');

    /**
     * Marking that client has js enabled.
     */
    Html.removeClass('no-js').addClass('js');

    /**
     * Bind the resize_end and resize_start early
     */
    Window.resizeEvents();

    /**
     * Binding on ajax event
     */
    Html
        .on('ajaxSend', function() {

            // Do something on ajax send event.

        })
        .on('ajaxComplete', function() {

            // Do something on ajax complete event
        });


    $.fn.builConnectingDots = function() {
        this.each(function() {
            var parent = $(this);

            if (parent.parent().children('canvas').length != 0) {
                parent.parent().children('canvas').remove();
            }

            parent.parent().prepend('<canvas />');

            var Canvas = parent.parent().children('canvas');

            Canvas[0].width = parent.width();
            Canvas[0].height = parent.height();

            parent.find('.items').each(function() {
                var element = $(this),
                    target = element.children('i');

                if (element.next().length != 0 && element.next().hasClass('items')) {
                    var startH = element.position().left + (target.width() / 2) + target.position().left,
                        startV = element.position().top + (target.height() / 2) + target.position().top,
                        endH = element.next().position().left + (element.next().children('i').width() / 2) + element.next().children('i').position().left,
                        endV = element.next().position().top + (element.next().children('i').height() / 2) + element.next().children('i').position().top;

                    Canvas.drawLine({
                        strokeStyle: target.css('border-left-color'),
                        strokeWidth: target.css('border-left-width'),
                        x1: startH,
                        y1: startV,
                        x2: endH,
                        y2: endV
                    });

                    Canvas.drawArc({
                        fillStyle: element.css('border-left-color'),
                        radius: 10,
                        x: (startH + endH) / 2,
                        y: (startV + endV) / 2
                    });
                }

            });
        });
    };

    /**
     * Window events
     */
    Window
        .on('load', function() {

            // Fix round thumbnail
            // This is needed for webkit browsers
            RoundThumbs.each(function() {
                var self = $(this),
                    width = self.innerWidth() - 9;

                self.find('img').width(width).height(width);
            });

            // Liquid Carousel
            Liquid.each(function() {
                var elHeight = $(this).find('img').eq(0).height();
                $(this).liquidcarousel({
                    height: elHeight
                });
            });

            // Load Sticky Header
            $('#header').hcSticky({
                onStart: function() {
                    $(this).addClass('slideInDown');
                },
                onStop: function() {
                    $(this).removeClass('slideInDown');
                }
            });

            // Resize Post icon
            var size = $('.post-icon-icon').width() / 2,
                bubble = $('.post-icon-bubble');

            $('.post-icon-icon i').css('font-size', size);

            bubble.each(function() {
                if ($(this).width() < 60) {
                    $(this).find('.post-icon-slogan').hide();
                    $(this).addClass('no-text');
                } else {
                    $(this).find('.post-icon-slogan').show();
                    $(this).removeClass('no-text');
                }

                var posticon = $(this).find('.post-icon-icon');
                posticon.css('line-height', (posticon.height() - 8) + 'px');
            });


            // Build simple gallery
            if (Galleries.length != 0) {
                Galleries.each(function() {
                    var Parent = $(this),
                        Thumbs = [];

                    Parent.children().each(function() {
                        Thumbs.push($(this).find('img').clone().attr('width', 64).attr('height', 64));
                    });

                    Parent.addClass('fotorama').fotorama({
                        width: '100%',
                        fit: 'none',
                        nav: ($(this).attr('data-thumbnail') == 'enabled') ? 'thumbs' : 'none',
                        ratio: $(this).find('img:eq(0)').outerWidth() + '/' + $(this).find('img:eq(0)').outerHeight()
                            //height: '100%'
                    });

                    $.each(Thumbs, function(key, val) {
                        $('.fotorama__nav__frame').eq(key).children().append(val);
                    });

                });

            }

            // Build Maps
            if (Maps.length != 0 && $.fn.goMap) {

                var maps = window['googlemaps'],
                    options = {};

                options = {
                    address: maps['center'],
                    zoom: parseInt(maps['zoom']),
                    mapTypeControl: (maps['map_type_control'] == '1') ? true : false,
                    mapTypeControlOptions: {
                        position: maps['map_type_control_position'],
                        style: maps['map_type_control_style']
                    },
                    scaleControl: (maps['scale_control'] == '1') ? true : false,
                    scrollwheel: (maps['scrollable'] == '1') ? true : false,
                    streetViewControl: (maps['street_view'] == '1') ? true : false,
                    markers: [{
                        address: maps['center']
                    }],
                    maptype: maps['types'],
                    draggable: maps['draggable'],
                    disableDoubleClickZoom: (maps['disable_zoom'] == '1') ? false : true
                }

                if (maps['responsive'] == '1') {
                    var p = Maps.parent(),
                        innerWidth = p.innerWidth(), // Need to add extra 32px 
                        innerHeight = p.innerHeight();

                    if (p.hasClass('post-thumbnail')) {
                        innerWidth += 32;
                        innerHeight += 32;
                    }
                    Maps.width(innerWidth).height(innerHeight);

                }

                Maps.goMap(options);
            }

            // Navigation slick nav
            $('#navigation').find('.nav').slicknav({
                label: '',
                appendTo: '#header'
            });

            var Menu = $('#header').find('.slicknav_nav');

            Menu.find('.dropdown').removeClass('dropdown');
            Menu.find('.dropdown-menu').removeClass('dropdown-menu');
            Menu.find('.dropdown-toggle').removeClass('dropdown-toggle');
            Menu.find('.caret').remove();


            // Ui To Top scroller
            Html.UItoTop({
                easingType: 'easeOutQuart'
            });

            // Equalheight per row
            // @note equalheight must run before masonry and isotope.
            if (EqualRow.length != 0) {
                setTimeout(function() {
                    EqualRow.resetEqualHeight(500);
                }, 500);
            }


            // Isotope filtering
            if (Isotope.length != 0) {

                // Bind the click event and sort the elements
                Isotope.on('click', '.term-link', function(e) {
                        // This must be before anything else
                        // To avoid page change if js is broken
                        e.preventDefault();

                        var self = $(this),
                            Element = $('.isotope-target[data-target=' + self.attr('data-target') + ']'),
                            sort = '[data-id*=' + self.attr('data-id') + ']';

                        if (self.attr('data-id') == '*') {
                            sort = '*';
                        }

                        $('.isotope-sources[data-target=' + self.attr('data-target') + ']').find('li').removeClass('active');
                        self.parent('li').addClass('active');

                        Element.isotope({
                            itemSelector: '.isotope-item',
                            layoutMode: 'fitRows',
                            filter: sort,
                            onLayout: function($elems, instance) {
                                //Window.trigger('resize_end');
                                var size = $elems.find('.post-icon-icon').width() / 2;
                                $elems.find('.post-icon-icon').css('font-size', size);
                            }
                        });
                    })
                    .on('click', '.grid-mode', function(e) {
                        e.preventDefault();

                        var self = $(this),
                            Element = $('.isotope-target[data-target=' + self.attr('data-target') + ']');

                        Element
                            .removeClass('grid')
                            .removeClass('list')
                            .addClass(self.attr('data-class'))
                            .resetEqualHeight(500)
                            .isotope('reLayout');

                        $('.grid-active').removeClass('grid-active');
                        self.parent().addClass('grid-active');

                    });
            }


            if ($('.forcefullwidth_wrapper_tp_banner').length != 0) {
                Window.trigger('resize');
            }

            // Dotline drawing line using twojs
            if (DotLine.length != 0) {
                setTimeout(function() {
                    DotLine.builConnectingDots();
                }, 1400);
            }
        })
        .on('scroll', function() {

            // Put some code here to act on scroll event

        })
        .on('resize_end', function() {

            // Fix round thumbnail
            // This is needed for webkit browsers
            RoundThumbs.each(function() {
                var self = $(this),
                    width = self.innerWidth() - 9;

                self.find('img').width(width).height(width);
            });

            //$('#header').hcSticky('reinit');

            // Equalheight per row
            if (EqualRow.length != 0) {
                EqualRow.resetEqualHeight(500);
            }

            // Resize Post icon
            var size = $('.post-icon-icon').width() / 2,
                bubble = $('.post-icon-bubble');

            $('.post-icon-icon i').css('font-size', size);

            bubble.each(function() {
                if ($(this).width() < 60) {
                    $(this).find('.post-icon-slogan').hide();
                    $(this).addClass('no-text');
                } else {
                    $(this).find('.post-icon-slogan').show();
                    $(this).removeClass('no-text');
                }

                var posticon = $(this).find('.post-icon-icon');
                posticon.css('line-height', (posticon.height() - 8) + 'px');
            });

            // Dotline drawing line using twojs
            if (DotLine.length != 0) {
                DotLine.builConnectingDots();
            }
        });

});