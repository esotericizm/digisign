$(document).ready(function() {
	var socket = io.connect('/');
	socket.emit('connected');

	var addr = $('#addr').html();
	var id = window.location.href.substring(window.location.href-20)

	var uri = 'digibyte:'+ addr +'?amount=1';
	var qrcode = new QRCode('qrcode', {
        text: uri,
        width: 256,
        height: 256,
        correctLevel : QRCode.CorrectLevel.H
    });

	setInterval(function() {
		socket.emit('awaitingPayment', addr);
	}, 10000);

    socket.on('notPaid', function(data){
    });

    socket.on('beenPaid', function(data){
    	location.reload();
    });
    
});