/**
 * Socket.io configuration
 */

'use strict';
var Document = require('./models/documents');
var request = require('request');

// When the user disconnects.. perform this
function onDisconnect(socket) {

}

// When the user connects.. perform this
function onConnect(socket) {

  socket.on('chat message', function(msg){
    socket.emit('chat message', {username: user.username, message: msg});
  });

  socket.on('newDoc', function(doc){
	  var id = doc.id;
	  var docName = doc.name;
	  var txMsg = doc.docHash;
	  var docType = doc.docType;
	  var prevArr = doc.previousDocs;
	  var timestamp = Math.floor(Date.now() / 1000);
	  Document.findOne({documentID: id}, function(err, found){
	    if(!found){
	    	var newDoc = new Document({documentID: id, name: docName, documentHash: doc.docHash, documentType: docType, timeAdded: timestamp, previousDocs: prevArr, isPrivate: doc.isPrivate});
	    	newDoc.createReceiveAddress(function(){
	    		newDoc.save(function(err){
	    			if(err) { console.log('error saving document')};
	    			socket.emit('docFound', {docID: id});
	    		});
	    	});
	    } else {
	      socket.emit('docFound', {docID: id});
	    }
	});
  });

  socket.on('awaitingPayment', function(addr){
  	var timestamp = Math.floor(Date.now() / 1000);
  	Document.findOne({generatedAddr: addr}, function(err, found){
  		found.checkUTXO(function(utxo){
  			if(!utxo){
  				socket.emit('notPaid', { addr: found.generatedAddr, documentHash: found.documentMessage });
  			} else {
				var txMsg;
				if(found.documentType === "None"){
					txMsg = found.documentMessage;
				} else {
					txMsg = found.documentType + " " + found.documentMessage;
				}
				found.createTransaction(utxo, txMsg, function(rawtx){
					if(rawtx === null){
						socket.emit('error', { error: 'You need to send a minimum of 2 DigiByte! '});
					} else {
						found.broadcastTransaction(rawtx, function(err){
							if (err) {
								socket.emit('error', { error: 'We couldn\'t broadcast your transaction, Please try again later! '});
							} else {
								found.txSent = true;
								found.timeBroadcasted = timestamp; 
								found.save(function(err, saved){
									if(err){console.log(err)};
									socket.emit('beenPaid', { transaction: found.txid, timeAdded: new Date(found.timeAdded * 1000), timeBroadcasted: new Date(timestamp * 1000) })
								});
							}
						});
					}
				});
  			}
  		})
 	})
  });

  socket.on('newMsg', function(doc){
	  var id = doc.id;
	  var txMsg = doc.message;
	  var timestamp = Math.floor(Date.now() / 1000);
	  Document.findOne({documentID: id}, function(err, found){
	    if(!found){
	    	var newDoc = new Document({documentID: id, documentMessage: doc.message, documentHash: doc.hash, timeAdded: timestamp, isPrivate: doc.isPrivate});
	    	newDoc.createReceiveAddress(function(){
	    		newDoc.save(function(err){
	    			if(err) { console.log('error saving document')};
	    			socket.emit('docFound', {docID: id});
	    		});
	    	});
	    } else {
	      socket.emit('docFound', {docID: id});
	    }
	});
  });

  socket.on('addPrevious', function(previousHash){
  	Document.findOne({documentHash: previousHash}, function(err, found){
  		console.log(found, previousHash)
  		if(!found){
  			socket.emit('foundPrevious', "Document Not Found");
  		} else {
  			socket.emit('foundPrevious', found.documentHash);
  		}
  	});
  });

  socket.on('search', function(searchText){
  	Document.find({documentID: searchText}, {'_id': 0, '__v': 0, 'privKey': 0}, function(err, found){
  		if(err) { console.log(err) }
  		if(found.length === 0){
  			Document.find({documentMessage: searchText}, {'_id': 0, '__v': 0, 'privKey': 0}, function(err, found2){
  				if(found2.length === 0){
				    request('http://digiexplorer.info/api/tx/' + searchText, function(err, res, body){
				      if(err) {  socket.emit('error', err); }
				      try {
				        var json = JSON.parse(body);
				        for (var o in json.vout){
					        var asm = json.vout[o].scriptPubKey.asm;
					        var OP_Return = "OP_RETURN";
					        if(asm.indexOf(OP_Return) > -1){
						        asm = asm.substring(10);
						        var msgString = '';
							    for (var i = 0; i < asm.length; i += 2) {
							        msgString += String.fromCharCode(parseInt(asm.substr(i, 2), 16));
							    }
							}
						}
					    socket.emit('onSearch', { found: true, data: msgString, txConversion: true});
				      } catch(e){
				        socket.emit('onSearch', {found: false });
				      }
				    });
  				} else if(found2.isPrivate) {
  					socket.emit('onSearch', {found: false});
  				} else {
  					socket.emit('onSearch', {found: true, data: found2});
  				}
  			});
  		} else if(found.isPrivate) {
  			socket.emit('onSearch', "");
  		} else {
  			delete found.privKey;
  			delete found._id;
  			delete found.__v;
  			delete found.generatedAddr;
  			socket.emit('onSearch', { found: true, data: found});
  		}
  	})
  });
}

module.exports = function (socketio, app) {


  socketio.on('connection', function (socket) {
  	//		(socket)

    socket.connectedAt = new Date();
    onConnect(socket);
  });

};