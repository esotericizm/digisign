var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
var bitcore = require('bitcore');
var request = require('request');
 
var DocumentsSchema = new Schema({
	  documentID: { type: String, default: null, index: true },
    name: { type: String, index: true, default: null },
    documentHash: { type: String, default: null },
  	documentMessage: { type: String, default: null },
  	documentType: { type: String, default: null },
  	generatedAddr: { type: String, default: null },
  	privKey: { type: String, default: null },
  	txSent: { type: Boolean, default: false },
  	txid: { type: String, index: true },
  	timeAdded: { type: Number },
  	timeBroadcasted: { type: Number },
    previousDocs: {type: Array, default: [] },
    isPrivate: { type: Boolean, default: false }
}, {id: false});

DocumentsSchema.methods = {

  createReceiveAddress: function(cb){
    var self = this;
    self.privKey = "";
    self.generatedAddr = "";
    var privateKey = new bitcore.PrivateKey();
    var newAddr = privateKey.toAddress();
    self.privKey = privateKey;
    self.generatedAddr = newAddr;
    return cb()
  },

  checkUTXO: function(cb){
    var self = this;
    request('http://digiexplorer.info/api/addr/' + self.generatedAddr + '/utxo', function(err, res, body){
      if(err) { console.log(err) }
        try {
          var json = JSON.parse(body);
          return cb(json[0]);
        } catch(e){
          return cb(e)
        }
    });
  },

  createTransaction: function(utxo, hash, cb){
    var self = this;
    if (utxo.amount < 2){
      return cb(null);
    } else {
      var amount = (utxo.amount *100000000) - 100000000;
      var opReturnTx = new bitcore.Transaction()
      .from(utxo)
      .to(self.generatedAddr, amount)
      .addData(hash)
      .fee(100000000)
      .sign(self.privKey)
      var txSerialized = opReturnTx.serialize(true);
      return cb(txSerialized);
    }
  },

  broadcastTransaction: function(rawtx, cb){
    var self = this;
    request.post('http://digiexplorer.info/' + 'api/tx/send', { form: {rawtx: rawtx}}, function(err, res, body){
      if(err){ return cb(err) };
        try {
          var json = JSON.parse(body);
          self.txid = json.txid;
          self.txSent = true;
          return cb()
        } catch(e){
          return cb(e);
        }
    });
  }

};



module.exports = mongoose.model('Documents', DocumentsSchema);
