var express = require('express');
var router = express.Router();
var util = require("util");
var fs = require("fs"); 
var crypto = require('crypto');
var bitcore = require('bitcore');
var request = require('request');
var Document = require('../models/documents');
var async = require('neo-async')

var DocTypes = [
	{ name: "None", code: null },
	{ name: "Purchase Order  (POR)", code: "POR" }, 
	{ name: "CMR Document (CMR)", code: "CMR" },
	{ name: "Commercial Invoice (CIV)", code: "CIV" },
	{ name: "Airway Bill (AWB)", code: "AWB" },
	{ name: "Packing List (PLT)", code: "PLT" },
	{ name: "Bill of Lading (BOL)", code: "BOL" },
	{ name: "Letter of Credit (LOC)", code: "LOC" },
	{ name: "Multimodal Bill of Lading (FBL)", code: "FBL" },
	{ name: "Certificate of Origin (COO)", code: "COO" },
	{ name: "Inspection Certificate (ICT)", code: "ICT" },
	{ name: "Insurance Document (IDC)", code: "IDC" },
	{ name: "Tax Document (TDC)", code: "TDC" },
	{ name: "Real Estate Document (RDC)", code: "RDC" },
	{ name: "Estate Document (EDC)", code: "EDC" },
	{ name: "Bank Account Document (BDC)", code: "BDC" },
	{ name: "Identification Document (IDT)", code: "IDT" },
	{ name: "Contract Document (CDC)", code: "CDC" },
	{ name: "Legal Document (LGD)", code: "LGD" },
	{ name: "Non Disclosure Agreement (NDA)", code: "NDA" },
	{ name: "Employment Agreement (EAT)", code: "EAT" },
	{ name: "Memorandum of Understanding (MOU)", code: "MOU" },
	{ name: "Terms of Use (TOU)", code: "TOU" },
	{ name: "Privacy Policy (PPY)", code: "PPY" },
	{ name: "Business Plan (BPL)", code: "BPL" },
	{ name: "Operating Agreement (OAT)", code: "OAT" }
]

// Deletes documents that havent been paid for after 30 minutes -- curently not set to execute unless you uncomment the function below it
function deleteExpiredRequests(cb){
	var timestamp = Math.floor(Date.now() / 1000);
	Document.find({txSent: false}, function(err, found){
		for (var i in found){
			var a = timestamp - found[i].timeAdded;
			if ((timestamp - found[i].timeAdded) >= 86400){
				found[i].remove(function(err, removed){
					return cb();
				});
			}
		}
	});
}

function checkNonSent(cb){
	var timestamp = Math.floor(Date.now() / 1000);
	Document.find({txSent: false}, function(err, found){
		async.eachSeries(found, function(doc, callback){
			if(doc.txSent){
				callback();
			} else {
				doc.checkUTXO(function(utxo){
					if(utxo.amount >= 2){
						doc.createTransaction(utxo, doc.documentHash, function(rawtx){
							doc.broadcastTransaction(rawtx, function(){
								doc.timeBroadcasted = timestamp;
								doc.save(function(err){
									callback();
								});
							});
						});
					} else {
						callback();
					}
				});
			}
		}, function(err){
			return cb();
		})
	});
}

function makeid(){
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for( var i=0; i < 20; i++ )
	    text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

// The timer and execution of the above function
setTimeout(function() {
	deleteExpiredRequests(function(removed){
	});
}, 600000 * 24);

setTimeout(function() {
	checkNonSent(function(){});
}, 20000);


/* GET home page. */
router.get('/', function(req, res, next) {
  	res.render('index', { types: DocTypes });
});

router.get('/api/register/:hash', function(req, res, next) {
	var id = makeid();
	var timestamp = Math.floor(Date.now() / 1000);
	var newDoc = new Document({documentID: id, documentHash: req.params.hash, timeAdded: timestamp});
	newDoc.createReceiveAddress(function(){
		newDoc.save(function(err){
			res.json({"success": true, "hash": newDoc.documentHash, "address": newDoc.generatedAddr, "amount": 2, "timeAdded": timestamp});
		});
	});
});

router.get('/api/status/:hash', function(req, res, next) {
	var id = makeid();
	Document.findOne({documentHash: req.params.hash}, {"_id": 0, "__v": 0, "privKey": 0 }, function(err, found){
		if(found){
			if(!found.txSent){
				var obj = {
					success: true,
					documentHash: found.documentHash,
					address: found.generatedAddr,
					txSent: found.txSent,
					timeAdded: found.timeAdded,
				}
				res.json(obj)
			} else {
				var obj = {
					success: true,
					documentHash: found.documentHash,
					txSent: found.txSent,
					txid: found.txid,
					timeAdded: found.timeAdded,
					timeBroadcast: found.timeBroadcasted
				}
				res.json(obj);
			}
		} else {
			res.json({"found": false});
		}
	})
});

router.get('/document/:id', function(req, res, next){
	var timestamp = Math.floor(Date.now() / 1000);
	Document.find({documentID: req.params.id}, {'_id': 0, '__v': 0, 'privKey': 0}, function(err, found){
		if(found.length === 0 ){
			Document.find({documentHash: req.params.id}, {'_id': 0, '__v': 0, 'privKey': 0}, function(err, found2){
				if(found2.length === 0){
					res.render('404')
				} else if (found2[0].txSent){
					res.render('broadcasted', { doc: found2 });
				} else if (found[0].documentHash) {
					res.render('payments', { addr: found2[0].generatedAddr });
				}
			});
		} else if (found[0].txSent){
			res.render('broadcasted', { doc: found });
		} else if (found[0].documentHash) {
			res.render('payments', { addr: found[0].generatedAddr });
		}
	});
});

router.get('/recent', function(req, res, next) {
	Document.find({}, {'_id': 0, '__v': 0}).sort('-timeBroadcasted').limit(10).exec(function(err, docs){
  		res.render('recent', { recentMsgs: docs });
  	});
});

router.get('/faq', function(req, res, next) {
  	res.render('faq');
});

router.get('/search', function(req, res, next) {
	res.render('search', { documents: null });
});

router.get('/about', function(req, res, next) {
  	res.render('about');
});

module.exports = router;
